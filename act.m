function y = act(x,tflag,args)

G = args{1};
dims = args{2};
mask = args{3};
if length(args) >= 5
  Cx = args{4};
  Cy = args{5};
else
  Cx = [];
  Cy = [];
end
if length(args) == 7
  Ix = args{6};
  Iy = args{7};
else
  Ix = [];
  Iy = [];
end

if isempty(Cx)
  if strcmp(tflag,'transp')
    y = reshape(x(:),dims);
    y = ifftshift(ifft(ifftshift(y,1),[],1),1)*dims(1);
    y = mask(:).*double(G'*y(:));
  else
    y = double(reshape(G*(mask(:).*x(:)),dims));
    y = fftshift(fft(fftshift(y,1),[],1),1);
    y = y(:);
  end
elseif isempty(Ix)
  if strcmp(tflag,'transp')
    yx = reshape(x(1:prod(dims)),dims);
    yx = ifftshift(ifft(ifftshift(yx,1),[],1),1)*dims(1);
    yx = Cx'*double(G'*yx(:));
    yy = reshape(x(prod(dims)+1:end),dims);
    yy = ifftshift(ifft(ifftshift(yy,1),[],1),1)*dims(1);
    yy = Cy'*double(G'*yy(:));    
    y = mask(:).*(yx + yy);
  else
    yx = double(reshape(G*double(Cx*(mask(:).*x(:))),dims));
    yx = fftshift(fft(fftshift(yx,1),[],1),1);
    yy = double(reshape(G*double(Cy*(mask(:).*x(:))),dims));
    yy = fftshift(fft(fftshift(yy,1),[],1),1);
    y = [yx(:);yy(:)];
  end  
else
  if strcmp(tflag,'transp')
    yx = reshape(x,dims).*Ix;
    yx = ifftshift(ifft(ifftshift(yx,1),[],1),1)*dims(1);
    yx = Cx'*double(G'*yx(:));
    yy = reshape(x,dims).*Iy;
    yy = ifftshift(ifft(ifftshift(yy,1),[],1),1)*dims(1);
    yy = Cy'*double(G'*yy(:));    
    y = mask(:).*(yx + yy);
  else
    yx = double(reshape(G*double(Cx*(mask(:).*x(:))),dims));
    yx = fftshift(fft(fftshift(yx,1),[],1),1);
    yy = double(reshape(G*double(Cy*(mask(:).*x(:))),dims));
    yy = fftshift(fft(fftshift(yy,1),[],1),1);
    y = Ix.*yx+Iy.*yy;
    y = y(:);
  end    
end