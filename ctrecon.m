% Step 1: do a simple CG-based CT recon 

% make an object containing a circle and a couple squares
dim = 64; 
[x,y] = meshgrid(-dim/2:dim/2-1);
circrad = dim/2.5;
circ = x.^2 + y.^2 <= circrad.^2;
sq1w = dim/8;
sq2w = dim/16;
sq1c = [-9 -9];
sq2c = [20 0];
sq1 = abs(x-sq1c(1)) <= sq1w/2 & abs(y-sq1c(2)) <= sq1w/2;
sq2 = abs(x-sq2c(1)) <= sq2w/2 & abs(y-sq2c(2)) <= sq2w/2;

obj = sq1 + sq2 + circ;

% smooth the object
objsm = conv2(obj,exp(-(x.^2 + y.^2)),'same');

mask = x.^2 + y.^2 <= (dim/2)^2;
%mask = true(dim);

% calculate projections
Np = ceil(pi*dim/2);
theta = [0:Np-1]*pi/Np;
kx = [-dim/2:dim/2-1]'*cos(theta);
ky = [-dim/2:dim/2-1]'*sin(theta);
G = Gmri([kx(:) ky(:)],true(dim),'fov',1);

data = reshape(G*objsm,size(kx));
data = fftshift(fft(fftshift(data,1),[],1),1);

% run lsqr to recon image from the projection data
rec = lsqr(@(x,tflag)act(x,tflag,{G,size(kx),mask}),data(:));
%rec = embed(rec,mask);
rec = reshape(rec,[dim dim]);

% Step 2: Add centered difference operators to it, before the projection
% [-1 1]/2 + [-1 1]/2
% assume that image is zero outside boundary
Cx = sparse(diag(-1/2*ones(dim-1,1),-1) + diag(1/2*ones(dim-1,1),1));
Cx = kron(speye(dim),Cx);
Cy = sparse(diag(-1/2*ones(dim*(dim-1),1),-dim) + diag(1/2*ones(dim*(dim-1),1),dim));

datax = double(reshape(G*(Cx*objsm(:)),size(kx)));
datax = fftshift(fft(fftshift(datax,1),[],1),1);
datay = double(reshape(G*(Cy*objsm(:)),size(kx)));
datay = fftshift(fft(fftshift(datay,1),[],1),1);

Cxm = Cx;%Cx(mask(:),mask(:));
Cym = Cy;%Cy(mask(:),mask(:));
foo = {G,size(kx),mask,Cxm,Cym};
recd = lsqr(@(x,tflag)act(x,tflag,foo),[datax(:);datay(:)],[],100);
%recd = embed(recd,mask);
recd = reshape(recd,[dim dim]);

% Step 3: Compute displacements from the projections at each angle,
%         and recon from that.
Ix = repmat(cos(theta),[dim 1]);
Iy = repmat(sin(theta),[dim 1]);

foo = {G,size(kx),mask,Cxm,Cym,Ix,Iy};
datab = Ix.*datax + Iy.*datay;
recb = lsqr(@(x,tflag)act(x,tflag,foo),datab(:),[],100);
recb = reshape(recb,[dim dim]);

% generate figures!
% want to show: 
% 1) original index of refraction field (n)
% 2) x and y derivatives of n
% 3) projections of derivatives (sinogram) - may not be informative
% 4) displacement sinogram
% 5) example displaced image
% 6) reconstructed n and error
figure
im(objsm)
axis off,title ''
savefig(gcf,'fig,ctrec,obj');
figure
im(reshape(Cx*objsm(:),[dim dim]));
axis off,title ''
savefig(gcf,'fig,ctrec,xdiff');
figure
im(reshape(Cy*objsm(:),[dim dim]));
axis off,title ''
savefig(gcf,'fig,ctrec,ydiff');
figure
im(real(datax));
axis off,title ''
savefig(gcf,'fig,ctrec,xdiffproj');
figure;
im(real(datay));
axis off,title ''
savefig(gcf,'fig,ctrec,ydiffproj');
figure;
im(real(datab));
axis off,title ''
savefig(gcf,'fig,ctrec,dispproj');
figure
im(real(recb))
axis off,title ''
savefig(gcf,'fig,ctrec,recon');
figure;
im(abs(real(recb)-objsm));
axis off,title ''
savefig(gcf,'fig,ctrec,error');

% make an image of shifted lines
l = ones(dim*4,1);
l(2:2:end) = 0;
xi = 0:dim*4-1;
d = interp1(0:dim-1,real(datab(:,dim/2)),[0:1/4:dim-1/4],'linear',0);
xis = xi + d/max(abs(datab(:)))*4;
li = interp1(xi,l,xis,'linear',0);
l = repmat(l,[1 dim]);
li = repmat(li',[1 dim]);



