% load a zebrography acquisition, duplicate and backproject it
fusoff = imread('060.jpg');
fuson = imread('210.jpg');

fusoff = sqz(mean(fusoff,3));
fuson = sqz(mean(fuson,3));

zeb = fuson - fusoff;
zeb = circshift(zeb,[241-185 0]);
zeb = zeb(151:340,:);
zeb = circshift(zeb,[4 0]);

% Step 1: do a simple CG-based CT recon 

dim = size(zeb);

return

% calculate projections
Np = ceil(pi*dim(1)/2);
theta = [0:Np-1]*pi/Np;
kx = [-dim(1)/2:dim(1)/2-1]'*cos(theta);
ky = [-dim(1)/2:dim(1)/2-1]'*sin(theta);
G = Gmri([kx(:) ky(:)],true(dim(1)),'fov',1);

mask = true(dim(1));

% for each line, run lsqr to recon image from the projection data
for ii = 1:dim(2)
  ii
  foo = lsqr(@(x,tflag)act(x,tflag,{G,size(kx),mask}),col(repmat(zeb(:,ii),[1 Np])),0.01,40);
  %rec = embed(rec,mask);
  rec(:,:,ii) = reshape(foo,[dim(1) dim(1)]);
  
  save progress
end

exit

w = smooth3(rec);

p = patch(isosurface(flipdim(permute(w,[1 3 2]),2),5));
>> clf
>> p = patch(isosurface(flipdim(permute(w,[1 3 2]),2),5));
>> set(p,'facecolor','red')
>> set(p,'edgealpha',0)    
>> view(3)
>> daspect([1,1,1])
>> camlight
>> camorbit(0,90)
>> view(3); axis tight                                    
>> camlight left
>> axis off



